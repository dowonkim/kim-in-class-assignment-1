<!DOCTYPE html>
<head>
<title>Battlefield Analyze</title>
<style type="text/css">
body{
	width: 760px; /* how wide to make your web page */
	background-color: teal; /* what color to make the background */
	margin: 0 auto;
	padding: 0;
	font:12px/16px Verdana, sans-serif; /* default font */
}
div#main{
	background-color: #FFF;
	margin: 0;
	padding: 10px;
}
h1{
	text-align: center;
}
</style>
</head>
<body><div id="main">
<?php
	if (isset($_POST['ammo']) && isset($_POST['soldiers']) && isset($_POST['duration']) && isset($_POST['critique'])){
		$am = (int) $_POST['ammo'];
		$sold = (int) $_POST['soldiers'];
		$dura = (int) $_POST['duration'];
		$crit = $_POST['critique'];

		$mysqli = new mysqli('id', 'ammunition', 'soldiers', 'duration', 'critique', 'posted');

		if($mysqli->connect_errno) {
			printf("Connection Failed: %s\n", $mysqli->connect_error);
			exit;
		}
    
		
	<h1>Battlefield Analysis</h1>
	<h2>Latest Critiques</h2>
		
	$stmt = $mysqli->prepare("select critique from reports order by posted");
	
	if(!$stmt){
		printf("Query Prep Failed: %s\n", $mysqli->error);
		exit;
	}
	
	$stmt->execute();
 
	$stmt->bind_result($crit);
	 
	echo "<ul>\n";
	while($stmt->fetch()){
		printf("\t<li>%s %s</li>\n",
			htmlspecialchars($crit),
		);
	}
	echo "</ul>\n";
	 
	$stmt->close();
	
	<h2>Battle Statistics</h2>
	
	$stmt = $mysqli->prepare("select avg(ammunition) from reports group by soldiers order by soldiers");
	
	if(!$stmt){
		printf("Query Prep Failed: %s\n", $mysqli->error);
		exit;
	}
	
	$stmt->execute();
	
	<table border="1">
	<tr>
	<th>Number of Soldiers</th>
	<th>Pounds of Ammunition per Second</th>
	</tr>
	<tr>
	<td>row 1, cell 1</td>
	<td>row 1, cell 2</td>
	</tr>
	<tr>
	<td>row 2, cell 1</td>
	<td>row 2, cell 2</td>
	</tr>
	</table>
?>
		
<a href="battlefield-submit.html">Submit a New BAttle Report</a>

</div></body>
</html>