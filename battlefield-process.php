<!DOCTYPE html>
<head><title>Battlefield Process</title></head>
<body>
<?php

if (isset($_POST['ammo']) && isset($_POST['soldiers']) && isset($_POST['duration']) && isset($_POST['critique'])){
	$am = (int) $_POST['ammo'];
	$sold = (int) $_POST['soldiers'];
	$dura = (int) $_POST['duration'];
	$crit = $_POST['critique'];

	$mysqli = new mysqli('id', 'ammunition', 'soldiers', 'duration', 'critique', 'posted');

	if($mysqli->connect_errno) {
		printf("Connection Failed: %s\n", $mysqli->connect_error);
		exit;
	}
    
	$stmt = $mysqli->prepare("insert into reports (ammunition, soldiers, duration, critique) values (?, ?, ?, ?)");
	
	if(!$stmt){ //print error message if query failed
		printf("Query Prep Failed: %s\n", $mysqli->error);
		exit;
	}
 
	$stmt->bind_param('ssss', $am, $sold, $dura, $crit);
 
	$stmt->execute();
 
	$stmt->close();
	
	header("Location: battlefield-submit.html"); //redirect to HTML page 
}

else {
	printf("Error, not all variables are present.");
	exit;
}
?>
</body>
</html>